const { app, BrowserWindow, globalShortcut, Menu } = require('electron')

function createWindow () {
  const win = new BrowserWindow({
    minWidth: 800,
    minHeight: 600,
    width: 1200,
    height: 800,
    icon: './assets/icon.png',
    webPreferences: {
      nodeIntegration: true
    }
  })

  win.loadFile('app/index.html')
  win.removeMenu()
}

app.whenReady().then(() => {
  createWindow()

  app.dock.setMenu(Menu.buildFromTemplate([
    {
      label: 'New Window',
      click () { createWindow() }
    }
  ]))
})

app.on('ready', async () => {
  if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
    await installExtensions()
  }
  globalShortcut.register('CommandOrControl+R', () => false)
  globalShortcut.register('F5', () => false)

  setMainMenu()
 }
)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


const setMainMenu = () => {
  const isMac = process.platform === 'darwin'
  
  const template = [
    {
      label: 'Dockerun',
      submenu: [
        {
          label: 'Open Dockerun in browser',
          accelerator: 'CmdOrCtrl+O',
          click: async () => {
            const { shell } = require('electron')
            await shell.openExternal('https://dockerun.francescosorge.com')
          }
        },
        {
          label: 'Open GitLab repository',
          click: async () => {
            const { shell } = require('electron')
            await shell.openExternal('https://gitlab.com/fsorge/dockerun-electron')
          }
        },
        {
          label: 'Open developer website',
          click: async () => {
            const { shell } = require('electron')
            await shell.openExternal('https://francescosorge.com')
          }
        },
        { type: 'separator' },
        { role: 'quit' }
      ]
    },
    { role: 'editMenu' },
    {
      label: 'Window',
      submenu: [
        { role: 'minimize' },
        { role: 'zoom' },
        ...(isMac ? [
          { type: 'separator' },
          { role: 'front' },
          { type: 'separator' },
          { role: 'window' }
        ] : [
          { role: 'close' }
        ])
      ]
    },
    {
      role: 'help',
      submenu: [
        {
          label: 'Learn More',
          click: async () => {
            const { shell } = require('electron')
            await shell.openExternal('https://gitlab.com/fsorge/dockerun-electron')
          }
        }
      ]
    },
  ]

  Menu.setApplicationMenu(Menu.buildFromTemplate(template))
}